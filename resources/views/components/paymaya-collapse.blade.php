<div class="row mt-3">
    <div class="col">
        <input type="text" class="form-control" placeholder="Card Number" name="cc_number" maxlength="16">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <select class="form-control" name="exp_month">
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>
    </div>
    <div class="col">
        <select class="form-control" name="exp_year">
            <option value="2019">2019</option>
            <option value="2020">2020</option>
            <option value="2021">2021</option>
            <option value="2022">2022</option>
            <option value="2023">2023</option>
            <option value="2024">2024</option>
            <option value="2025">2025</option>
            <option value="2026">2026</option>
            <option value="2027">2027</option>
            <option value="2028">2028</option>
            <option value="2029">2029</option>
        </select>
    </div>
    <div class="col">
        <input type="text" class="form-control" placeholder="CVC" name="cvc" maxlength="3">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <input type="email" class="form-control" placeholder="Email" name="email">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <input type="text" class="form-control" placeholder="First name" name="first_name">
    </div>
    <div class="col">
        <input type="text" class="form-control" placeholder="Last name" name="last_name">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <input type="text" class="form-control" placeholder="Address" name="address">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <input type="text" class="form-control" placeholder="City" name="city">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <input type="text" class="form-control" placeholder="Region" name="region">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <select class="form-control" name="country">
            <option value="PH">Philippines</option>
        </select>
    </div>
    <div class="col">
        <input type="text" class="form-control" placeholder="Postal Code" name="postal_code">
    </div>
</div>

<div class="row mt-3">
    <div class="col">
        <input type="text" class="form-control" placeholder="Phone Number" name="phone_number">
    </div>
</div>
