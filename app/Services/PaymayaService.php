<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;
use Illuminate\Http\Request;

class PaymayaService
{
    use ConsumesExternalServices;

    protected $baseUri;
    protected $public_key;
    protected $secret_key;

    public function __construct()
    {
        $this->baseUri = config('services.paymaya.base_uri');
        $this->public_key = config('services.paymaya.public_key');
        $this->secret_key = config('services.paymaya.secret_key');
    }

    // public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    // {
    //     $headers['Authorization'] = $this->resolveAccessToken();
    // }

    public function decodeResponse($response)
    {
        return json_decode($response);
    }

    // public function resolveAccessToken()
    // {
    //     return 'Basic ' . base64_encode($this->secret_key);
    // }

    public function handlePayment(Request $request)
    {
        if ($request->currency != 'php') {
            return redirect()->route('home')->withErrors('Currency is not supported.');
        } else {
            $paymentToken = $this->createPaymentToken(
                $request->cc_number,
                $request->exp_month,
                $request->exp_year,
                $request->cvc
            );

            session()->put('paymentTokenId', $paymentToken->paymentTokenId);

            $createPayment = $this->createPayment(
                $paymentToken->paymentTokenId,
                $request->value,
                $request->currency,
                $request->email,
                $request->first_name,
                $request->last_name,
                $request->address,
                $request->city,
                $request->region,
                $request->country,
                $request->postal_code,
                $request->phone_number,
            );

            session()->put('paymentId', $createPayment->id);
            return redirect($createPayment->verificationUrl);
        }
    }

    public function handleApproval()
    {
        if (session()->has('paymentId')) {
            return redirect()->route('home')->with('success', ['payment' => "We have received your payment."]);
        }

        return redirect()->route('home')->withErrors('We are unable to process the payment. Please try again.');
    }

    public function createPayment($paymentTokenId, $amount, $currency, $email, $firstName, $lastName, $address, $city, $region, $country, $postalCode, $phoneNumber)
    {
        return $this->makeRequest(
            'POST',
            '/payments/v1/payments',
            [],
            [
                'paymentTokenId' => $paymentTokenId,
                'totalAmount' => [
                    'amount' => $amount,
                    'currency' => strtoupper($currency),
                ],
                'buyer' => [
                    'firstName' => $firstName,
                    'lastName' => $lastName,
                    'contact' => [
                        'phone' => $phoneNumber,
                        'email' => $email,
                    ],
                    'billingAddress' => [
                        'line1' => $address,
                        'line2' => '',
                        'city' => $city,
                        'state' => $region,
                        'zipCode' => $postalCode,
                        'countryCode' => $country,
                    ],
                ],
                "redirectUrl" => [
                    "success" => "http://localhost:8000/payments/success",
                    "failure" => "http://localhost:8000/payments/failed",
                    "cancel" => "http://localhost:8000/payments/cancelled",
                ],
            ],
            [
                'Authorization' => 'Basic ' . base64_encode($this->secret_key),
            ],
            $isJsonRequest = true,
        );
    }

    public function createPaymentToken($cc, $mm, $yyyy, $cvc)
    {
        return $this->makeRequest(
            'POST',
            "/payments/v1/payment-tokens",
            [],
            [
                "card" => [
                    "number" => $cc,
                    "expMonth" => $mm,
                    "expYear" => $yyyy,
                    "cvc" => $cvc,
                ],
            ],
            [
                'Authorization' => 'Basic ' . base64_encode($this->public_key),
            ]
        );
    }

    public function resolveFactor($currency)
    {
        $zeroDecimalCurrencies = ['JPY'];

        if (in_array(strtoupper($currency), $zeroDecimalCurrencies)) {
            return 1;
        }

        return 100;
    }

}
